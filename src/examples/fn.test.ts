import fn2, {fn1} from './fn'

jest.mock('./fn', () => {
  const originalModule = jest.requireActual('./fn')
  return {
    __esModule: true,
    ...originalModule,
    default: jest.fn((a, b) => a * b * 10),
    fn1() { return 'this is mocked fn1 function.' },
  }
})

describe("Mock functions - 3", () => {
  test("test jest.fn invoke", () => {
    let mockFn = jest.fn()
    let res = mockFn(1, 2, 3)
    expect(res).toBeUndefined()
    expect(mockFn).toBeCalled()
    expect(mockFn).toBeCalledTimes(1)
    expect(mockFn).toHaveBeenCalledWith(1, 2, 3)

    const myMock = jest.fn(() => true)
    myMock.mockImplementation(() => false)
  })

  test("test jest.fn return fix values", () => {
    let mockFn = jest.fn().mockReturnValue('default')
    expect(mockFn()).toBe('default')
  })

  test("test jest.fn internal implement", () => {
    let mockFn = jest.fn((num1, num2) => {
      return num1 * num2
    })
    expect(mockFn(10, 20)).toBe(200)
  })

  test("test jest.fn return promise", async () => {
    let mockFn = jest.fn().mockResolvedValue('default')
    let res = await mockFn()
    expect(res).toBe('default')
    expect(Object.prototype.toString.call(mockFn())).toBe("[object Promise]")
  })

  test('mock partial fn module', () => {
    expect(fn1()).toEqual('this is mocked fn1 function.')
    expect(fn2(2, 3)).toEqual(60)
  })

  test('mock a implement of fn2', () => {
    jest.mock('./fn')
    const mockFn2 = require('./fn').default
    mockFn2.mockImplementation((a: number, b: number) => a * b)
    expect(mockFn2(2, 3)).toBe(6)
  })
})