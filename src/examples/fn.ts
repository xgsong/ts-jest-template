export const fn1 = () => {
  return "this is fn1 function."
}

export default function fn2(a: number, b: number) {
  return a + b
}

export function fn3() {
  return "this is fn3 function."
}