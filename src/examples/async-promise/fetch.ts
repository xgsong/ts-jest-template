export const fetchApple = (callback: Function) => {
  setTimeout(() => callback("apple"), 2000);
};
