const users = {
  4: { name: "Mark" },
  5: { name: "Billy" },
};

export default function request(url: string) {
  return new Promise((resolve, reject) => {
    const userId = parseInt(url.substring("/users/".length), 10);
    process.nextTick(() => {
      users[userId]
        ? resolve(users[userId])
        : reject({ error: `User with id ${userId} not found` });
    });
  });
}
