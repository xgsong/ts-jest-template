import { fetchApple } from "./fetch";

test("the data is apple", (done) => {
  expect.assertions(1); // 验证当前测试中有1处断言会被执行，在测试异步代码时，能确保回调中的断言被执行。
  const callback = (data: string) => {
    expect(data).toBe("apple");
    done();
  };
  fetchApple(callback);
});
