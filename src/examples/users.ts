import axios from 'axios'

class Users {
  static all(): any {
    return axios.get('/users.json').then(resp => resp.data);
  }
}

export default Users