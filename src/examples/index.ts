function hello(name = "") {
  if(name.length === 0) {
    return "Hello Jest."
  } else {
    return `Hello Jest. My name is ${name}.`
  }
}

export default hello