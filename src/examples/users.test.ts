import axios from "axios"
import Users from "./users"

jest.mock('axios')
const mockedAxios = axios as jest.Mocked<typeof axios>

describe('Mock functions - 2', () => {
  test('should fetch users', () => {
    const users = [{name: 'Billy'}]
    const resp = {data: users}
    mockedAxios.get.mockResolvedValue(resp)
    return Users.all().then((data: any) => expect(data).toEqual(users));
  })
})