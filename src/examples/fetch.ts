import axios from "axios";

async function fetchPostsList(callback: Function) {
  return axios.get('https://jsonplaceholder.typicode.com/posts').then(res => {
    return callback(res.data)
  })
}

export default fetchPostsList