import axios from "axios"
import hello from "./index"
import Users from "./users"

describe("Test hello function", () => {
  test("hello fn with name", () => {
    expect(hello("Billy Song")).toBe("Hello Jest. My name is Billy Song.")
  })

  test("hello fn without name", () => {
    expect(hello()).toBe("Hello Jest.")
  })
})

describe("Mock functions - 1", () => {
  test("Mock return values", () => {
    const forEach = (items: number[], callback: Function) => {
      for(let idx = 0; idx < items.length; idx++) {
        callback(items[idx])
      }
    }
    const mockCallback = jest.fn(x => x + 1)
    forEach([0, 1], mockCallback)
    expect(mockCallback.mock.calls.length).toBe(2)

    const myMock = jest.fn();
    console.log(myMock());
    myMock.mockReturnValueOnce(10).mockReturnValueOnce('x').mockReturnValueOnce(true);
    console.log(myMock(), myMock(), myMock(), myMock());

    const filterTestFn = jest.fn();
    filterTestFn.mockReturnValueOnce(true).mockReturnValueOnce(false);
    const result = [11, 12].filter(num => filterTestFn(num));
    console.log(result); // [11]
    console.log(filterTestFn.mock.calls[0][0]); // 11
    console.log(filterTestFn.mock.calls[1][0]); // 12
  })
})